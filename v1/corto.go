package v1

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"time"

	"github.com/thought-machine/gonduit"
	"github.com/thought-machine/gonduit/entities"
	"github.com/thought-machine/gonduit/requests"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
)

type Corto struct {
	phabConn  *gonduit.Conn
	gdriveSrv *drive.Service
	config    *Config
	// phabProject string # which phab project to search for incidents
	templates *template.Template
}

func New(config *Config, phab *gonduit.Conn, gdrive *drive.Service) (c *Corto, err error) {
	t, err := template.ParseFS(config.TemplatesFS, "task.*.tmpl")
	if err != nil {
		return nil, fmt.Errorf("unable to parse templates: %w", err)
	}
	c = &Corto{config: config, phabConn: phab, gdriveSrv: gdrive, templates: t}
	return c, nil
}

func (c *Corto) CreateIncident(ctx context.Context, title string) (incident *Incident, err error) {
	var descriptionBuf bytes.Buffer

	err = c.templates.ExecuteTemplate(&descriptionBuf, "task.description.tmpl", title)
	if err != nil {
		return nil, fmt.Errorf("unable to create task description: %w", err)
	}

	PHID, err := c.createTask(title, descriptionBuf.String())
	if err != nil {
		return nil, fmt.Errorf("unable to create task: %w", err)
	}

	task, err := c.findTaskByPHID(PHID)
	if err != nil {
		return nil, fmt.Errorf("unable to find task: %w", err)
	}
	fmt.Printf("Phab URI: %s\n", task.URI)
	gdocTitle := title + " T" + task.ID
	gdoc, err := c.createTmplGdoc(gdocTitle)
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve files: %w", err)
	}
	fmt.Printf("Gdoc URI: %s\n", gdoc.WebViewLink)

	i := Incident{
		Id:         task.ID,
		Status:     Open,
		Started_at: time.Time(task.DateCreated),
		Title:      task.Title,
		Public:     false,
	}

	// TODO: Add gdoc to phab task as an attachment
	return &i, nil
}

func (c *Corto) ListOpen(ctx context.Context) (res bool, err error) {
	// TODO list all open incidents in Phabricator
	return true, err
}

func (c *Corto) ResolveIncident(ctx context.Context, PHID string) (res bool, err error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO resolve task
	return true, err
}

func (c *Corto) TitleIncident(ctx context.Context, PHID string, new_title string) (res bool, err error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO change title
	return true, err
}

func (c *Corto) UpdateIC(ctx context.Context, PHID string, new_ic string) (res bool, err error) {
	// task, err := c.findTaskByPHID(PHID)
	// if err != nil {
	// 	return false, fmt.Errorf("Unable to find task: %w", err)
	// }
	// TODO set IC
	return true, err
}

// TODO: fill in templated values in doc after copying, e.g. <NAME>
func (c *Corto) createTmplGdoc(gdocTitle string) (gdoc *drive.File, err error) {
	fileSrv := c.gdriveSrv.Files
	gdoc = &drive.File{
		Name:    gdocTitle,
		Parents: []string{c.config.GoogleDriveId},
	}
	call := fileSrv.Copy(c.config.GoogleDocTemplateId, gdoc)
	gdoc, err = call.Do()
	if err != nil {
		return nil, err
	}

	// Re-get our file, so we can grab the webViewLink field, alternatively, we could
	// just construct the url ourselves and hope the format never changes.
	fields := []googleapi.Field{"webViewLink"}
	gdoc, err = fileSrv.Get(gdoc.Id).Fields(fields...).Do()
	if err != nil {
		return nil, err
	}

	return gdoc, nil
}

// findTaskByPHID returns the Phabricator task with the given PHID string
func (c *Corto) findTaskByPHID(PHID string) (task *entities.ManiphestTask, err error) {
	res, err := c.phabConn.ManiphestQuery(requests.ManiphestQueryRequest{PHIDs: []string{PHID}})
	if err != nil {
		return nil, err
	}

	for _, v := range *res {
		return v, nil
	}
	return nil, fmt.Errorf("no task with PHID %s found", PHID)
}

// findTaskByID returns the Phabricator task with the given ID string
func findTaskByID(conn *gonduit.Conn, id string) (task *entities.ManiphestTask, err error) {
	res, err := conn.ManiphestQuery(requests.ManiphestQueryRequest{IDs: []string{id}})
	if err != nil {
		return nil, err
	}

	for _, v := range *res {
		return v, nil
	}
	return nil, fmt.Errorf("no task with id %s found", id)
}

func (c *Corto) createTask(title, taskBody string) (PHID string, err error) {
	transactions := []entities.Transaction{
		{Type: "title", Value: title},
		{Type: "description", Value: taskBody},
		{Type: "status", Value: "open"},
	}
	res, err := c.phabConn.ManiphestEdit(requests.EditRequest{
		Transactions: transactions})
	if err != nil {
		return "", err
	}
	PHID = string(res.Object.PHID)
	return PHID, nil
}
