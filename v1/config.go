package v1

import "embed"

type Config struct {
	GoogleDriveId       string
	GoogleDocTemplateId string
	TemplatesFS         *embed.FS
}
