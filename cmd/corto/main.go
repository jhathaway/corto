package main

import (
	"context"
	"log"
	"os"

	"github.com/thought-machine/gonduit"
	"github.com/thought-machine/gonduit/core"
	"gitlab.wikimedia.org/repos/sre/corto/templates"
	corto "gitlab.wikimedia.org/repos/sre/corto/v1"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/option"
)

var gdocTmplId = "1OeRtaRCJsEVfQjztAKllGs8cA5HyNH4pjXiLnDwfep4"
var driveDirId = "1t6YRP6Y3fJJu4sUA5T7cyrdoRlkASuHi"

func main() {

	phabTokenBytes, err := os.ReadFile("phab-token.txt")
	if err != nil {
		log.Fatalf("Unable to read phab token: %v", err)
	}

	phabToken := string(phabTokenBytes)

	phabConn, err := gonduit.Dial(
		"https://phabricator.wikimedia.org",
		&core.ClientOptions{APIToken: phabToken},
	)
	if err != nil {
		log.Fatalf("Unable to connect to phab: %v", err)
	}

	ctx := context.Background()
	gdriveSrv, err := drive.NewService(ctx, option.WithCredentialsFile("gdrive-creds.json"))
	if err != nil {
		log.Fatalf("Unable to obtain Google Drive client: %v", err)
	}

	cfg := corto.Config{
		GoogleDriveId:       driveDirId,
		GoogleDocTemplateId: gdocTmplId,
		TemplatesFS:         &templates.FS,
	}

	c, _ := corto.New(&cfg, phabConn, gdriveSrv)

	incident, err := c.CreateIncident(ctx, "CORTOTEST3")
	if err != nil {
		log.Fatalf("Unable to create incident: %v", err)
	}

	log.Printf("Created incident %v", incident)
}
